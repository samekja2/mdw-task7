package mdw;

import java.util.Hashtable;
import java.util.List;
import java.util.Set;

public class DB {
    private static DB instance = null;
    private Hashtable<Integer, Booking> bookings = new Hashtable();
 
    public static DB getInstance() {
        if (instance == null)
            instance = new DB();
        return instance;
    }
    public void addBooking(Booking b){
        bookings.put(b.getId(), b);
    }
    public Booking getGet(int id) {
        return bookings.get(id);
    }
    
    public Set<Integer> getIds() {
    	return bookings.keySet();
    }
    
    public boolean bookingExists(int id) {
    	return bookings.containsKey(id);
    }
    
    public String getBookings() {
    	String allBookings = "";
    	for (int id : bookings.keySet()) {
    	  allBookings = allBookings + "\n" + bookings.get(id);
  		}
    	return allBookings;
    }
    
    public void deleteBooking(int id) {
    	bookings.remove(id);
    	}
    
    
}
