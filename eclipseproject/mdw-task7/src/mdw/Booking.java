package mdw;

public class Booking {
    private String passengerName;
    private String departure;
    private String arrival;
    
    private String from;
    private String to;
    
    private int id;
    

	public Booking(String passengerName, String departure, String arrival, String from, String to, int id) {
		super();
		this.passengerName = passengerName;
		this.departure = departure;
		this.arrival = arrival;
		this.from = from;
		this.to = to;
		this.id = id;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Booking [passengerName=" + passengerName + ", departure=" + departure + ", arrival=" + arrival
				+ ", from=" + from + ", to=" + to + ", id=" + id + "]";
	}
    
    
}
