package mdw;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class BookingService {
    
    public String addBooking(@WebParam(name="bookingId") int bookingId, @WebParam(name="passengerName") String passengerName,
    		@WebParam(name="departureTime") String departure,
    		@WebParam(name="arrivalTime") String arrival,
    		@WebParam(name="fromAirport") String from,
    		@WebParam(name="toAirport") String to) {
    	DB db = DB.getInstance();
    	Booking booking = new Booking(passengerName, departure, arrival, from, to, bookingId);
        db.addBooking(booking);
    	return "Booking created.";
    }
    
    public String getBookings() {
    	DB db = DB.getInstance();
    	return db.getBookings();
    }
    
    public String editBooking(@WebParam(name="bookingId") int bookingId, @WebParam(name="passengerName") String passengerName,
    		@WebParam(name="departureTime") String departure,
    		@WebParam(name="arrivalTime") String arrival,
    		@WebParam(name="fromAirport") String from,
    		@WebParam(name="toAirport") String to) {
    	DB db = DB.getInstance();
    	
    	
    	if (db.bookingExists(bookingId)) {
    		Booking b = db.getGet(bookingId);
    		b.setArrival(arrival);
    		b.setDeparture(departure);
    		b.setFrom(from);
    		b.setTo(to);
    		b.setPassengerName(passengerName);
    		return "Booking edited.";
    	} else {
    		return "Booking not found.";
    	}
    }
    
    public String deleteBooking(@WebParam(name="bookingId") int bookingId) {
    	DB db = DB.getInstance();
    	
    	if (db.bookingExists(bookingId)) {
    		db.deleteBooking(bookingId);
    		return "Booking deleted.";
    	} else {
    		return "Booking not found.";
    	}
    	
    }
}

